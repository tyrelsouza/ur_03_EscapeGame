// Tyrel Souza 2018

#include "OpenDoor.h"
#include "Engine/World.h"
#include "Components/PrimitiveComponent.h"
#include "GameFramework/Actor.h" // See auto completes

#define OUT


// Sets default values for this component's properties
UOpenDoor::UOpenDoor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame. 
	// You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UOpenDoor::BeginPlay()
{
	Super::BeginPlay();
	OwningDoor = GetOwner();

}

//Returns total mass in Kg
float UOpenDoor::GetTotalMassOfActorsOnPlate()
{
	float TotalMass = 0.0f;

	TArray<AActor*> OverlappingActors;

	//Find all overlapping actors.
	PressurePlate->GetOverlappingActors(
		OUT OverlappingActors
	);
	//iterate through finding masses.
	for (const auto *Actor : OverlappingActors) {
		UPrimitiveComponent *Component = Actor->FindComponentByClass<UPrimitiveComponent>();
		TotalMass += Component->GetMass();

		UE_LOG(LogTemp, Warning, TEXT("MASS %s"), *Actor->GetName());
	}


	return TotalMass;
}


void UOpenDoor::OpenDoor() {
	OwningDoor->SetActorRotation(FRotator(0.0f, OpenAngle, 0.0f));
}

void UOpenDoor::CloseDoor() {
	OwningDoor->SetActorRotation(FRotator(0.0f, 0.0f, 0.0f));
}

// Called every frame
void UOpenDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	float CurSeconds = GetWorld()->GetTimeSeconds();
	// poll the TriggerVolume every frame
	// if ActorThatOpens  is in the volume, open door
	if (GetTotalMassOfActorsOnPlate() > TriggerMass) {
		OpenDoor();
		LastDoorOpenTime = CurSeconds;
	}
	
	//check if time to close door
	if (CurSeconds > LastDoorOpenTime + DoorCloseDelay) {
		CloseDoor();
	}

}

