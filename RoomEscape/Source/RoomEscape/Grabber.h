// Tyrel Souza 2018

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "Components/InputComponent.h"
#include "Grabber.generated.h"


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class ROOMESCAPE_API UGrabber : public UActorComponent
{
	GENERATED_BODY()

public:
	UGrabber();
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

protected:
	virtual void BeginPlay() override;

private:
	float Reach = 100.f;

	UPhysicsHandleComponent* PhysicsHandle = nullptr;
	UInputComponent* InputComponent = nullptr;
	void Grab(); // Raycast and grab what is in reach
	void Release(); // Raycast and grab what is in reach

	// Find (assumed) attached physics handle
	void FindPhysicsHandleComponent();

	// Setup assumed or attached input component
	void SetUpInputComponent();

	// Return hit for first physics body in reach
	FHitResult GetFirstPhysicsBodyInReach() const;
	

	FVector PlayerViewpointLocation;
	FRotator PlayerViewpointRotation;

	FVector GetReachLineEnd() const;
	FVector GetReachLineStart() const;
	void SetPlayerLookingAt();
};
